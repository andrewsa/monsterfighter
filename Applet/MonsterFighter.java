/* Anthony Andrews
	3/3/13
	Final Project 1st Draft
	
	**PSEUDOCODE**
1. Create method to randomly generate monsters.
2. Create objects of type "Monster" from the Monster class.
3. Print introduction and ask user to pick a fighter.
4. Once user has picked a fighter, begin generating monsters -- each monster has a certain percentage
	chance that it will show up.
5. Ask user to attack, run, or check their stats for each round.
6. If attack, calculate damage done to both sides. If the user wins, the win is added to their kill count. 
	10 kills = game won.
7. If the user runs away, end the current round and begin a new one.
8. If the user loses all their HP, game over.
*/

import java.io.*;
import java.util.Scanner;
import java.util.Random;

public class MonsterFighter {

// Method for random number generator -- monster encounters will be based on this
public static int monsterGenerator() {
	Random randMonster = new Random();
	int whichMonsterAppears = randMonster.nextInt(101);
	return whichMonsterAppears;
	}
		
// The main routine
	public static void main (String[] args) throws IOException {
	
// Create 9 objects of type Monster & set their HP and attack power
	Monster cuteKitty = new Monster("Cute Kitty", 2, 1);
	Monster disgruntledDog = new Monster("Disgruntled Dog", 4, 2);
	Monster rabidRabbit = new Monster("Rabid Rabbit", 6, 3);
	Monster shiftySentry = new Monster("Shifty Sentry", 8, 4);
	Monster greedyGoblin = new Monster("Greedy Goblin", 10, 5);
	Monster waywardWerewolf = new Monster("Wayward Werewolf", 12, 6);
	Monster grandGriffin = new Monster("Grand Griffin", 14, 7);
	Monster dazzlingDragon = new Monster("Dazzling Dragon", 16, 8);
	Monster almightyArchmage = new Monster("Almighty Archmage", 18, 9);
	Monster currentMonster = new Monster(null, 1, 10); // Placeholder monster

// Print introduction
	Scanner keyboard = new Scanner(System.in);
	System.out.println("Welcome to Monster Fighter.");
	System.out.println("The object is to defeat 10 monsters in a row without dying.");	
	System.out.println("To get started, please select a fighter.");
	System.out.println("Your choices are Tank, Fencer, or Wizard.");
	System.out.println("Press t, f, or w to pick a fighter or press i if you need info on each fighter.");

// Prompt user to select a fighter to be assigned to variable
// User's HP and attack are based on what they select
	String userFighter = keyboard.nextLine();
	int userHP = 1;
	int userAttack = 1;
	
// Import the information on each fighter from a text file in case the user wants to see it
	File fighterInfo = new File("FighterInfo.txt");
	Scanner readFromFighter = new Scanner(fighterInfo);
	
// Switch statement: 1 of 3 fighters to be selected and stored in userFighter
	boolean flag = true;

	while(flag) {							// Breaks the loop if valid choice is selected or continues it if invalid choice
		switch(userFighter) {
			case "t": 						// Tank
				System.out.println("You have picked Tank.");
				userFighter = "Tank";
				userHP = 30;
				userAttack = 3;
				flag = false;
				break;
			case "f":						 // Fencer
				System.out.println("You have picked Fencer.");
				userFighter = "Fencer";
				userHP = 22;
				userAttack =5;
				flag = false;
				break;
			case "w": 						// Wizard
				System.out.println("You have picked Wizard");
				userFighter = "Wizard";
				userHP = 12;
				userAttack = 7;
				flag = false;
				break;
			case "i": 						// Print info about each fighter
				while (readFromFighter.hasNext()) {
					System.out.println(readFromFighter.nextLine());
				}
				System.out.println("Press t, f, or w to pick a fighter.");
				userFighter = keyboard.nextLine();
				break;
			default: 						// Any other key is pressed
				System.out.println("Invalid choice! Please press t, f, w, or i."); 
				userFighter = keyboard.nextLine();
		}
	}


// Start the battle and keep track of kills
	int killCount = 0;
	System.out.println("You leave town to begin your journey.");
	System.out.println("As night falls, many monsters appear.");
	System.out.println("...");
	System.out.println("...");
	System.out.println("...");
	
// Monsters attack based on RNG
// Stronger monster = less likely it will appear
	while (userHP >= 0) {
		String archMage = "a";
	
		if (monsterGenerator() >= 0 && monsterGenerator() <= 15) {
			currentMonster = cuteKitty;
		}
		else if (monsterGenerator() > 15 && monsterGenerator() <= 30) {
			currentMonster = disgruntledDog;
		}
		else if (monsterGenerator() > 30 && monsterGenerator() <= 55) {
			currentMonster = rabidRabbit;
		}
		else if (monsterGenerator() > 55 && monsterGenerator() <= 65) {
			currentMonster = shiftySentry;
		}
		else if (monsterGenerator() > 65 && monsterGenerator() <= 75) {
			currentMonster = greedyGoblin;
		}
		else if (monsterGenerator() > 75 && monsterGenerator() <= 85) {
			currentMonster = waywardWerewolf;
		}
		else if (monsterGenerator() > 85 && monsterGenerator() <= 93) {
			currentMonster = grandGriffin;
		}
		else if (monsterGenerator() > 93 && monsterGenerator() <= 97) {
			currentMonster = dazzlingDragon;
		}
		else if (monsterGenerator() > 97 && monsterGenerator() <= 100) {
			currentMonster = almightyArchmage;
			archMage = "Almighty Archmage";
		}
		else {
			currentMonster = cuteKitty;
		}
	
	if (archMage == "Almighty Archmage") {
		System.out.println("An " + currentMonster.getMonsterName() + " appears before you!");
		}
	else {
	System.out.println("A " + currentMonster.getMonsterName() + " appears before you!");
	}
	
	System.out.println("Press a to attack, r to run away, or i to check stats.");
	
// Declare variables & switch statement for battle choices
	boolean secondFlag = true;
	int restoreHP = currentMonster.getMonsterHP();
	String battleChoice = keyboard.nextLine();
	
	while (secondFlag) {
		switch (battleChoice) {
			case "a":			// Attack
				restoreHP -= userAttack;
					
						// Calculate damage output to both sides and print results
						System.out.println("You attack the " + currentMonster.getMonsterName() + 
												" for " + userAttack + " points of damage!");
						if (restoreHP <= 0) {
							System.out.println(currentMonster.getMonsterName() + " has been defeated!");	
							secondFlag = false;
							killCount += 1;
							restoreHP = currentMonster.getMonsterHP(); // Restore monster's HP for the next encounter
							}
						else {
						System.out.println(currentMonster.getMonsterName() + " attacks you for " + 
												currentMonster.getMonsterAttack() + " points of damage!");
						userHP -= currentMonster.getMonsterAttack();
						if (userHP <= 0) {
							System.out.println("You have been defeated! Better luck next time!");
							System.exit(0);
						}
						else {
						System.out.println("Your HP: " + userHP);
						System.out.println("Enemy's HP: " + restoreHP);
						System.out.println("Press a to attack or r to run away.");
						battleChoice = keyboard.nextLine();
						}
					}
				break;
				
			case "r":			// Run away
				System.out.println("You run from the " + currentMonster.getMonsterName() + ".");
				secondFlag = false;
				break;
				
			case "i":			// User's stats
				System.out.println("Your HP: " + userHP);
				System.out.println("Enemy's HP: " + restoreHP);
				System.out.println("Kill Count: " + killCount);
				System.out.println("Press a to attack, r to run away, or i to check your stats.");
				battleChoice = keyboard.nextLine();
				break;
				
			default:				// Invalid input
				System.out.println("Invalid choice! Please press a, r, or i."); 
				battleChoice = keyboard.nextLine(); 
			}
			
			if (killCount == 10) {	// Achieve win condition
				System.out.println("Congrats! You win!");
				System.exit(0);
		} 
	}
	
	System.out.println("...");
	System.out.println("...");
	System.out.println("...");
	
}

}}