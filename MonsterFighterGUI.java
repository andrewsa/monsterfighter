/* Monster Fighter GUI
	Anthony Andrews
	CS111 Final Project */

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.Scanner;
import java.util.Random;

public class MonsterFighterGUI extends JFrame {

	// Character attributes
	private int userHP;
	private int userAttack;
	private int killCount;
	
	// Monster attributes
	private int monsterHP;
	private int monsterAttack;
	private String currentMonster;
	
	// Components for 1st panel
	private JPanel panel;
	private JLabel selectFighterL;
	private JLabel wizardL;
	private ImageIcon image;
	private JLabel imageLabel;
	private JRadioButton tankB;
	private JRadioButton fencerB;
	private JRadioButton wizardB;
	private ButtonGroup buttonGroup;
	private JTextArea textArea;
	private JTextField userHPField;
	private JTextField userAttackField;
	private JTextField killCountField;
	private JTextField monsterHPField;
	private JButton fightB;
	
	// Components for 2nd panel
	private JPanel fightPanel;
	private JButton attackB;
	private JButton runB;
	private JScrollPane scroll;
	
	// Constants for window size
	private final int WINDOW_WIDTH = 480;
	private final int WINDOW_HEIGHT = 640;
	
	// Constructor
	public MonsterFighterGUI() {
		
		// Set title
		setTitle("Monster Fighter");
		
		// Set close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Build panel
		buildPanel();
		
		// Create layout for panels
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		// Add panel
		add(panel);
		
		// Size and display the window.    
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);  
	//	setResizable(false);
	}
	
	// buildPanel method
	private void buildPanel() {
	
		// Create text field for fighter descriptions
		textArea = new JTextArea(4,40);
		textArea.setText("Tough, but has low power.");
		textArea.setOpaque(false);
		textArea.setEditable(false);
		
		// Force scroll
		int x;
		textArea.selectAll();
		x = textArea.getSelectionEnd();
		textArea.select(x,x);
		
		// Set font for textArea
		Font font = new Font("Times New Roman", Font.PLAIN, 16);
		Font boldFont = new Font("Arial", Font.BOLD, 12);
		textArea.setFont(font);
		
		// Set other fields and initial text
		userHPField = new JTextField(13);
		userAttackField = new JTextField(12);
		killCountField = new JTextField(13);
		userHPField.setText("Your HP: 40");
		
		// Set font for value fields
		userHPField.setFont(boldFont);
		userAttackField.setFont(boldFont);
		killCountField.setFont(boldFont);
		
		userAttackField.setText("Your Attack: 3");
		killCountField.setText("Kill Count: 0/10");
		userHP = 40;
		userAttack = 3;
		userHPField.setEditable(false);
		userAttackField.setEditable(false);
		killCountField.setEditable(false);
		
		userHPField.setHorizontalAlignment(JTextField.CENTER);
		userAttackField.setHorizontalAlignment(JTextField.CENTER);
		killCountField.setHorizontalAlignment(JTextField.CENTER);
		
		// Monster HP
		monsterHPField = new JTextField(40);
		monsterHPField.setText("Monster's HP: 0");
		monsterHPField.setEditable(false);
		monsterHPField.setHorizontalAlignment(JTextField.CENTER);
		monsterHPField.setFont(boldFont);
		monsterHPField.setVisible(false);
		
		// Create labels
		selectFighterL = new JLabel("Select your fighter");
		
		// Create buttons for 1st panel
		tankB = new JRadioButton("Tank", true);
		fencerB = new JRadioButton("Fencer");
		wizardB = new JRadioButton("Wizard"); 
		fightB = new JButton("Fight!");
		fightB.setVerticalAlignment(JButton.BOTTOM);
		buttonGroup = new ButtonGroup();
		buttonGroup.add(tankB);
		buttonGroup.add(fencerB);
		buttonGroup.add(wizardB);
		
		// Add change listeners
		tankB.addChangeListener(new ButtonListener());
		fencerB.addChangeListener(new ButtonListener());
		wizardB.addChangeListener(new ButtonListener());
		fightB.addActionListener(new FightButtonListener());
		
		// Create image
		image = new ImageIcon("tank.png");
		imageLabel = new JLabel();
		imageLabel.setIcon(image);
		
		// Create panel and add components
		panel = new JPanel();
		panel.add(selectFighterL);
		panel.add(tankB);
		panel.add(fencerB);
		panel.add(wizardB);
		panel.add(imageLabel);
		panel.add(textArea);
		panel.add(monsterHPField);
		panel.add(userHPField);
		panel.add(userAttackField);
		panel.add(killCountField);
		panel.add(fightB);
	}
	
	// Event listener for Fight Button -- leaves 1st screen and goes to 2nd
	private class FightButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			buildFightPanel();
		}
	}
	
	// Build the fight panel
	private void buildFightPanel() {
	
		// Clear previous text
		textArea.setText(null);
		
		// Add scroll bar
		scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel.add(scroll);
		
		// Add Attack and Run buttons and their listeners
		attackB = new JButton("Attack");
		runB = new JButton("Run");
		panel.add(attackB);
		panel.add(runB);
		runB.addActionListener(new RunButtonListener());
		attackB.addActionListener(new AttackButtonListener());
		monsterHPField.setVisible(true);
		
		// Remove old buttons
		fightB.setVisible(false);
		tankB.setVisible(false);
		fencerB.setVisible(false);
		wizardB.setVisible(false);
		selectFighterL.setVisible(false);
		
		// Generate monster
		NewMonsterAppears m = new NewMonsterAppears();
	}
	
	// Event listener for Attack button
	private class AttackButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			// Reset monster HP upon character's attack		 
			monsterHP -= userAttack;
			monsterHPField.setText("Monster's HP: " + monsterHP);
			textArea.append("\nYou attack for " + userAttack + " points of damage!");
			
			// Display message and increase kill count when monster is defeated
			if (monsterHP <= 0) {
				textArea.append("\nYou defeated the " + currentMonster + "!\n");
				killCount++;
				killCountField.setText("Kill Count: " + killCount + "/10");
				
					// Display winning message if kill count reaches 10
					if (killCount == 10) {
						textArea.append("\n\nCongrats! You won!");
						monsterHPField.setVisible(false);
						attackB.setEnabled(false);
						runB.setEnabled(false);
						image = new ImageIcon("win.png");
						imageLabel.setIcon(image);
					}
					
					// Start new round if not
					else {
						NewMonsterAppears m = new NewMonsterAppears();
					}
			}
			else if (monsterHP > 0) {
				textArea.append("\nThe " + currentMonster + " attacks you for " + 
			                   monsterAttack + " points of damage!\n");
				if (userHP <= 5) {
					userHPField.setForeground(Color.RED);
				}
				
			// Reset character's HP when attacked
			userHP -= monsterAttack;
			userHPField.setText("Your HP: " + userHP);
			if (userHP <= 5) {
					userHPField.setForeground(Color.RED);
				}
			
			// Display losing message if character is defeated
			if (userHP <= 0) {
				userHPField.setText("Your HP: 0");
				userHPField.setForeground(Color.RED);
				textArea.append("\nYou have been defeated! Better luck next time!");
				monsterHPField.setVisible(false);
				attackB.setEnabled(false);
				runB.setEnabled(false);
				image = new ImageIcon("death.png");
				imageLabel.setIcon(image);
				panel.setBackground(Color.RED);
			}
			
			}
		}
	}
	
	// Event listener for Run button
	private class RunButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			// Start new round
			textArea.append("\nYou ran from the " + currentMonster + "!\n");
			NewMonsterAppears m = new NewMonsterAppears();		
		}
	}
		
	// Event listener for radio buttons
	private class ButtonListener implements ChangeListener {
		public void stateChanged(ChangeEvent e) {
			
			// Select fighter
			if (tankB.isSelected()) {
				image = new ImageIcon("tank.png");
				textArea.setText("Tough, but has low power.");
				userHPField.setText("Your HP: 40");
				userAttackField.setText("Your Attack: 3");
				userHP = 40;
				userAttack = 3;
			}
			else if (fencerB.isSelected()) {
				image = new ImageIcon("fencer.png");
				textArea.setText("Has average HP and power compared to the others.");
				userHPField.setText("Your HP: 25");
				userAttackField.setText("Your Attack: 5");
				userHP = 25;
				userAttack = 5;
			}
			else {
				image = new ImageIcon("wizard.png");
				textArea.setText("Has high attack power but low HP.");
				userHPField.setText("Your HP: 15");
				userAttackField.setText("Your Attack: 8");
				userHP = 15;
				userAttack = 8;
			}
				
         // Place image
			imageLabel.setIcon(image);
		}
	}
	
	// Mechanism to spawn new monsters -- FIRST activated when "Fight" is clicked and
		//	activated each succesive time a monster is defeated or ran away from
	private class NewMonsterAppears extends MonsterFighter {
		
		// Randomly generate monsters
		public NewMonsterAppears() {
		
			// Create variable to store random number
			int i = monsterGenerator();
			
			// Check certain ranges & determine which monster to generate
			if (i >= 0 && i <= 15) {
				currentMonster = "Cute Kitty";
				image = new ImageIcon("cuteKitty.png");
				imageLabel.setIcon(image);
				monsterHP = 2;
				monsterAttack = 1;
			}
			else if (i > 15 && i <= 34) {
				currentMonster = "Disgruntled Dog";
				image = new ImageIcon("disgruntledDog.png");
				imageLabel.setIcon(image);
				monsterHP = 4;
				monsterAttack = 2;
			}
			else if (i > 34 && i <= 44) {
				currentMonster = "Rabid Rabbit";
				image = new ImageIcon("rabidRabbit.png");
				imageLabel.setIcon(image);
				monsterHP = 6;
				monsterAttack = 3;
			}
			else if (i > 44 && i <= 54) {
				currentMonster = "Shifty Sentry";
				image = new ImageIcon("shiftySentry.png");
				imageLabel.setIcon(image);
				monsterHP = 8;
				monsterAttack = 4;
			}
			else if (i > 54 && i <= 64) {
				currentMonster = "Greedy Goblin";
				image = new ImageIcon("greedyGoblin.png");
				imageLabel.setIcon(image);
				monsterHP = 10;
				monsterAttack = 5;
			}
			else if (i > 64 && i <= 74) {
				currentMonster = "Wayward Werewolf";
				image = new ImageIcon("waywardWerewolf.png");
				imageLabel.setIcon(image);
				monsterHP = 12;
				monsterAttack = 6;
			}
			else if (i > 74 && i <= 84) {
				currentMonster = "Grand Griffin";
				image = new ImageIcon("grandGriffin.png");
				imageLabel.setIcon(image);
				monsterHP = 14;
				monsterAttack = 7;
			}
			else if (i > 84 && i <= 94) {
				currentMonster = "Dazzling Dragon";
				image = new ImageIcon("dazzlingDragon.png");
				imageLabel.setIcon(image);
				monsterHP = 16;
				monsterAttack = 8;
			}
			else if (i > 94 && i <= 100) {
				currentMonster = "Almighty Archmage";
				image = new ImageIcon("almightyArchmage.png");
				imageLabel.setIcon(image);
				monsterHP = 18;
				monsterAttack = 9;
			}
			else {
				currentMonster = "Cute Kitty";
				image = new ImageIcon("cuteKitty.png");
				imageLabel.setIcon(image);
				monsterHP = 2;
				monsterAttack = 1;
			}
			
			// Set monster name to text field
			if (currentMonster.equals("Almighty Archmage")) {
				textArea.append("\nAn Almighty Archmage appears before you!\n");
			}
			else {
				textArea.append("\nA " + currentMonster + " appears before you!\n");
			}
				monsterHPField.setText("Monster's HP: " + monsterHP);
		}
	}

	// Main method
	public static void main(String[] args) {
		MonsterFighterGUI mGUI = new MonsterFighterGUI();
		
		// Show welcome screen
		JOptionPane.showMessageDialog(null, "Welcome to Monster Fighter. The object of the game is to" +
												" defeat 10 monsters in a row without dying." +
												"\n\nTo get started, pick a fighter.");
	}
}