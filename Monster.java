/* Anthony Andrews
	3/3/13
	Final Project 1st Draft (Class)
*/

public class Monster  {			  //	The class to create objects of type Monster
	private String monsterName; 
	private int monsterHP;
	private int monsterAttack;
	
	
	public void setMonsterName(String name) {
		monsterName = name;
	}
	
	public String getMonsterName() {	// A method to return the name of the monster
		return monsterName;
	}
	
	public void setMonsterHP(int mHP) { // A method to set monster's HP
		monsterHP = mHP;							
	}																		
		
	public int getMonsterHP() {	// Return the monster's HP
		return monsterHP;
	}
	
	public void setMonsterAttack(int mAtk) { // A method to set monster's attack strength
		monsterAttack = mAtk;							
	}																		
		
	public int getMonsterAttack() {	// Return the monster's attack strength
		return monsterAttack;
	}

	
// Non-default constructor
	public Monster(String name, int mHP, int mAttack){
		monsterName = name;
		monsterHP = mHP;
		monsterAttack = mAttack;
	}	
}